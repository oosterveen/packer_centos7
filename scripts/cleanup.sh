#!/bin/bash
echo "==> Remove packages needed for building guest tools"
yum -y remove gcc cpp libmpc mpfr kernel-devel kernel-headers perl

echo "==> Clean up yum cache of metadata and packages to save space"
yum -y --enablerepo='*' clean all

yum clean all
